from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome import webdriver
from selenium import webdriver

from TestsMethods.openWebSite import openWebSite


class initWebDrive:
    def __init__(self, SELENIUM_HUB_HOST_chrome='http://selenium__standalone-chrome:4444/wd/hub'):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--no-sandbox')
        self.options.add_argument('--disable-dev-shm-usage')
        self.options.add_argument('--headless')
        self.options.add_argument('--disable-gpu')
        self.options.add_argument("--start-maximized")
        self.options.add_argument('window-size=1920,1080')
        self.driver = webdriver.Remote(command_executor=SELENIUM_HUB_HOST_chrome,
                                       desired_capabilities=DesiredCapabilities.CHROME, options=self.options)

        self.openWebSite = openWebSite(self)

    def endTests(self):
        self.driver.quit()
