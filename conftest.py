from confdriver import initWebDrive
import pytest


@pytest.fixture
def app(request):
    fixture = initWebDrive()
    request.addfinalizer(fixture.endTests)
    return fixture

