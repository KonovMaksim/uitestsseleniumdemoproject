import allure
import time


class dataWEbSite:
    url = "https://www.python.org/"


class openWebSite:
    def __init__(self, app):
        self.app = app

    def methodOpenWebSite(self):
        driver = self.app.driver
        driver.get(dataWEbSite.url)

    def methodGetScreenshot(self):
        driver = self.app.driver
        time.sleep(1)
        return allure.attach(driver.get_screenshot_as_png(), name='screenshot',
                             attachment_type=allure.attachment_type.PNG)

